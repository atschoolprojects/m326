import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'vocation'
})
export class VocationPipe implements PipeTransform {

  transform(value: string): string {
    switch (value) {
      case '45':
        return 'Informatiker/in EFZ Applikationsentwicklung';

      case '48':
        return 'Schreiner/in EFZ';

      case '55':
        return 'Hauswirtschaftspraktiker/in EBA';

      case '37':
        return 'Zeichner/in EFZ Fachrichtung Ingenieurbau';

      case '22':
        return 'Koch / Köchin EFZ';
      default:
        return 'Error';
    }
  }

}
