import { DisplayFormatPipe } from './displayFormat.pipe';

describe('DisplayFormatPipe', () => {
  it('T5: Should convert in Yes/No', () => {
    const pipe = new DisplayFormatPipe();
    expect(pipe.transform(false)).toBe('Nein');
  });

  it('T6: Should return the same number', () => {
    const pipe = new DisplayFormatPipe();
    expect(pipe.transform(11)).toBe(11);
  });

  it('T7: Should convert in HDD/SSD', () => {
    const pipe = new DisplayFormatPipe();
    expect(pipe.transform('ssd')).toBe('SSD');
  });

  it('T8: Should return Error', () => {
    const pipe = new DisplayFormatPipe();
    expect(pipe.transform('no_type')).toBe('Error');
  });

  it('T9: Should return no value', () => {
    const pipe = new DisplayFormatPipe();
    expect(pipe.transform({})).toBe('Keine Angabe');
  });
});
