import { Pipe, PipeTransform } from '@angular/core';
import { StorageType } from 'src/app/Models/storageType';

@Pipe({
  name: 'displayFormat'
})
export class DisplayFormatPipe implements PipeTransform {

  transform(value: any): string | number {
    if (typeof value === 'boolean') {
      if (value === true) {
        return 'Ja';
      } else if (value === false) {
        return 'Nein';
      }
    }

    else if (typeof value === 'number' && value > 0) {
      return value;
    }

    else if (typeof value === 'string') {
      if (value === StorageType.HDD) {
        return 'HDD';
      } else if (value === StorageType.SSD) {
        return 'SSD';
      } else {
        return 'Error';
      }
    } 
    
    else {
      return 'Keine Angabe';
    }
  }

}
