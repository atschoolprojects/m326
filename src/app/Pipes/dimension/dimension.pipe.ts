import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dimension'
})
export class DimensionPipe implements PipeTransform {

  transform(value: string): string {
    switch (value) {
      case 'processor_core_count':
        return 'Anz. Prozessorkerne';

      case 'processor_frequency_ghz':
        return 'Taktfrequenz (GHz)';

      case 'memory_gb':
        return 'Arbeitsspeicher/RAM (GB)';

      case 'storage_capacity_gb':
        return 'Speicherplatz (GB)';

      case 'display_size_inch':
        return 'Bildschirmgrösse (Zoll)';

      case 'storage_type':
        return 'Festplatte';

      case 'touchscreen':
        return 'Touchscreen';

      case 'pen':
        return 'Stift';

      case 'usb_c':
        return 'USB-C Anschluss';

      case 'ethernet':
        return 'Ethernet Anschluss';

      default:
        return 'Error';
    }
  }

}
