import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DeviceSpec } from '../../Models/DeviceSpec';

@Injectable({
  providedIn: 'root'
})
export class DevicespecService {

  private readonly jsonURL = 'assets/data/device-spec.json';

  constructor(
    private http: HttpClient
  ) { }

  // get the JOSN-File content
  get deviceSpecs() {
    return this.http.get<DeviceSpec[]>(this.jsonURL);
  }
}

