import { StorageType } from "src/app/Models/storageType"

export class DeviceSpecMock {
    get deviceSpecs() {
      return {
        "vocation_id": "45",
        "vocation_title": "Informatiker/in EFZ Applikationsentwicklung",
        "spec": {
          "required": {
            "processor_core_count": 2,
            "processor_frequency_ghz": 2,
            "memory_gb": 4,
            "storage_capacity_gb": 200,
            "display_size_inch": 13,
            "storage_type": StorageType.HDD,
            "touchscreen": false,
            "pen": false,
            "usb_c": false,
            "ethernet": true
          },
          "recommended": {
            "processor_core_count": 4,
            "processor_frequency_ghz": 2.2,
            "memory_gb": 8,
            "storage_capacity_gb": 250,
            "display_size_inch": 15,
            "storage_type": StorageType.SSD,
            "touchscreen": true,
            "pen": true,
            "usb_c": true,
            "ethernet": true
          }
        }
      }
    }
  }