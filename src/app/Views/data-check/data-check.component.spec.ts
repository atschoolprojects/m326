import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { VocationPipe } from 'src/app/Pipes/vocation/vocation.pipe';
import { DimensionPipe } from 'src/app/Pipes/dimension/dimension.pipe';
import { DataCheckComponent } from './data-check.component';

describe('DataCheckComponent', () => {
  let component: DataCheckComponent;
  let fixture: ComponentFixture<DataCheckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [ 
        DataCheckComponent,
        VocationPipe,
        DimensionPipe
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  function initialize(value: number) {
    component.componentsForm.get('core_count').setValue(value);
    component.validateUserInput('core_count', [1, 2, 3, 4, 6, 8]);
  }

  it('T12: Should calculate the number of Gridcols', () => {
    component.calculateCols(500);
    expect(component.gridCols).toBe(1);
  });

  it('T13: Should correct the userinput', () => {
    initialize(5);
    expect(component.componentsForm.get('core_count').value).toBe(4);
  });

  it('T14: Should let the userinput be the same', () => {
    initialize(6);
    expect(component.componentsForm.get('core_count').value).toBe(6);
  });

  it('T15: Should correct to low userinput', () => {
    initialize(0);
    expect(component.componentsForm.get('core_count').value).toBe(1);
  });
});
