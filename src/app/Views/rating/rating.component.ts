import { Component } from '@angular/core';
import { TableElement } from 'src/app/Models/TableElement';
import { DevicecomponentService } from 'src/app/Services/devicecomponent/devicecomponent.service';
import { DeviceComponents } from '../../Models/DeviceComponents';
import { DeviceSpec } from '../../Models/DeviceSpec';
import { DevicespecService } from '../../Services/devicespec/devicespec.service';
import { faCheck, faMinus, faTimes, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { Animations } from '../../Animations/rating.animations';
import { EChartsOption } from 'echarts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css'],
  animations: [
    Animations.markSize,
    Animations.detailsVisibility
 ]
})

export class RatingComponent {

  deviceComponents: DeviceComponents;
  vocationId: string;
  mark: number;

  showWarning = false;
  showDetails = false;
  showDiagram = false;

  tableDataSource: TableElement[] = [];
  displayedColumns = ['dimension', 'deviceComponent', 'result'];

  diagramDataReq = [];
  diagramDataRec = [];
  diagramDataUser = [];

  dynamicData: EChartsOption = {};

  fatick = faCheck;
  fastroke = faMinus;
  facross = faTimes;
  fawarn = faExclamationTriangle;

  // default specs
  allDevicespecs: DeviceSpec[] = [{
    vocation_id: '-1',
    vocation_title: 'Error',
    spec: {
      required: {},
      recommended: {}
    }
  }];

  // default specs of one device
  devicespecs: DeviceSpec = this.allDevicespecs[0];


  initialChartRadar: EChartsOption = {
    legend: {
      data: ['Mindestanforderungen', 'Empfohlen', 'Dein Gerät'],
      orient: 'vertical',
      left: 'left'
    },
    radar: {
      indicator: [
        { name: 'Anz. Porzessorkenre', max: 8 },
        { name: 'Taktfrequenz', max: 4 },
        { name: 'Arbeitsspeicher/RAM', max: 32},
        { name: 'Speicherplatz', max: 2056 },
        { name: 'Bildschirmgrösse', max: 17.3 },
        { name: 'Festplatte', max: 2 },
        { name: 'Touchscreen', max: 2 },
        { name: 'Stift', max: 2 },
        { name: 'USB-C Anschluss', max: 2 },
        { name: 'Ethernet Anschluss', max: 2 }
      ]
    },
    series: [
      {
        type: 'radar',
        data: []
      }
    ],
  };

  constructor(
    private devicespecService: DevicespecService,
    private devicecomponentService: DevicecomponentService,
    private router: Router
  ) {
    this.vocationId = devicecomponentService.vocationId;
    this.deviceComponents = devicecomponentService.deviceComponents;

    if (this.vocationId !== '-1') {
      // Data from service availabel
      devicespecService.deviceSpecs.subscribe({
        next: res => { this.allDevicespecs = res; this.compare(); this.createDiagramArrays(); },
        error: () => alert('Cannot get JSON')
      });
    } else {
      // The user shouldn't see the rating site if he hasn't filld in his device components
      this.router.navigate(['/data-check']);
    }
  }

  /**
   * Compares the users device with the requirements of his vocation and calculates a mark for the device
   */
  compare() {
    const result: DeviceComponents[] = [{}, {}, {}, {}]; // 0 -> tick, 1 -> stroke, 2 -> cross, 3 -> unknown
    let points = 0;
    let knwonDivisions = 0;

    this.devicespecs = this.allDevicespecs.find(specs => specs.vocation_id === this.vocationId); // get specs from the selected vocation

    /**************************
     * Compare each component *
     *************************/
    Object.entries(this.deviceComponents).forEach(dimension => { // dimension example => ['display_size_inch, 13]
      const ratingIndex = this.getRatingIndex(dimension);
      const componentValue = this.deviceComponents[dimension[0]];

      result[ratingIndex][dimension[0]] = componentValue; // set the current component at the calculated position
      
      // Add the current component to the table array
      this.tableDataSource.push(
        {
          dimension: dimension[0],
          deviceComponent: componentValue,
          result: ratingIndex
        }
      );

      // Add the current component to the diagram array
      this.diagramDataUser.push(this.toNumber(componentValue));
      
    });


    /**********************
     * Calculate the mark *
     *********************/

    // Foreach tick
    Object.keys(result[0]).forEach(() => {
      knwonDivisions ++;
      points += 1;
    });

    // Foreach stroke
    Object.keys(result[1]).forEach(() => {
      points += 0.6;
      knwonDivisions ++;
    });

    // Foreach cross
    Object.keys(result[2]).forEach(() => {
      knwonDivisions ++;
    });

    Object.keys(result[3]).forEach((dimension) => {
      if (dimension) {
        this.showWarning = true;
      }
    })

    this.mark = (points * 5) /  knwonDivisions + 1; // mark calculated by -> (your Points * 5) / max Points + 1

  }

  /**
   * Calculates how good the component is compared to the requirements.
   * 0: Tick, 1: Stroke, 2: Cross, 3: Usercomponent unknown
   * @param dimension Array with the name and value of the dimension: ['display_size_inch', 13]
   * @returns The index where the component has to be inserted in the result array
   */
  getRatingIndex(dimension: any): number {
    const componentName = dimension[0];
    const deviceComponent = this.deviceComponents[componentName];
    const requiredSpec = this.devicespecs.spec.required[componentName];
    const recommendedSpec = this.devicespecs.spec.recommended[componentName];

    if (deviceComponent === 0 || deviceComponent === null) {
      return 3;
    }

    switch (typeof dimension[1]) {
      case 'object':
        // the users component is unknown
        return 3; // unknown array

      case 'number':
        if (deviceComponent >= recommendedSpec) {
          // device >= rec >= req
          return 0; // tick

        } else if (deviceComponent >= requiredSpec) {
          // rec > device >= req
          return 1; // stroke

        } else  if (deviceComponent < requiredSpec) {
          // rec >= req > device
          return 2; // cross
        }
        break;

      case 'boolean':
        if (deviceComponent === true) {
          // device >= rec >= req
          // doesn't matter what the req/rec is, it's at least equal
          return 0; // tick

        } else if (deviceComponent === false && requiredSpec === true) {
          // rec = req > device
          return 2; // cross

        }
        // rec > device >= req
        return 1; // stroke
        

      case 'string':
        if (deviceComponent === recommendedSpec) {
          // device = rec >= req
          return 0; // tick
        }

        if (recommendedSpec === 'hdd' && deviceComponent === 'ssd') {
          // device > rec = req
          return 0;
        }

        if (recommendedSpec === 'ssd' && deviceComponent === 'hdd' && requiredSpec === 'hdd') {
          // rec > device = req
          return 1; // stroke
        }

        // rec >= req > decive
        return 2; // cross

      default:
        return 3; // unknown array
    }
  }

  /**
   * Collects all needed data and pushes them in the correct format into the array for the diagram
   */
  createDiagramArrays() {

    /*****************
     * Collects data *
     ****************/
    // Foreach required spec
    Object.values(this.devicespecs.spec.required).forEach(req => {
      this.diagramDataReq.push(this.toNumber(req)); // pushes the spec into the required array
    });

    // Foreach recommended spec
    Object.values(this.devicespecs.spec.recommended).forEach(rec => {
      this.diagramDataRec.push(this.toNumber(rec)); // pushes the spec into the recommended array
    });


    /*******************
     * Updates diagram *
     ******************/
    this.dynamicData = this.initialChartRadar;
    this.dynamicData.series = [
      {
        type: 'radar',
        data: [
          { name: 'Mindestanforderungen', value: this.diagramDataReq },
          { name: 'Empfohlen', value: this.diagramDataRec },
          { name: 'Dein Gerät', value: this.diagramDataUser }
        ]
      }
    ];
  }

  /**
   * Converts non numberic values into a number which can be used by the diagram
   * @param value Value that should be converted if nessecary
   * @returns The value if it's a number, 1 if the value is false of hdd, 2 if the value is true or another string than hdd 
   */
  toNumber(value: any): number{
    if (typeof value === 'boolean') {
      return value === false ? 1 : 2;
    } else if (typeof value === 'string') {
      return value === 'hdd' ? 1 : 2;
    }
    return value;
  }

  /**
   * Inverts the showDetails variable
   */
  triggerAnimation() {
    this.showDetails = !this.showDetails;
  }

  /**
   * Invert the showDiagram variable
   */
  changeView() {
    this.showDiagram = !this.showDiagram;
  }
}
