import { state, style, transition, animate, trigger } from '@angular/animations';


export const Animations = {
    markSize: trigger('markSize', [
        state('bigMark', style({
           height: '100%',
           'font-size': '350px',
        })),
        state('smallMark', style({
           height: '8%',
           'font-size': '20px',
        })),
        transition('bigMark => smallMark', [
           animate('0.25s')
        ]),
        transition('smallMark => bigMark', [
           animate('0.25s')
        ])
      ]),

      detailsVisibility: trigger('detailsVisibility', [
         state('showDetails', style({
            opacity: '1'
         })),
         state('hideDetails', style({
            opacity: '0'
         })),
         transition('showDetails => hideDetails', [
            animate('0.25s')
         ]),
         transition('hideDetails => showDetails', [
            animate('0.25s')
         ])
       ]),
}
