import { StorageType } from './storageType';

export interface DeviceComponents {
    processor_core_count?: number;
    processor_frequency_ghz?: number;
    memory_gb?: number;
    storage_capacity_gb?: number;
    display_size_inch?: number;
    storage_type?: StorageType;
    touchscreen?: boolean;
    pen?: boolean;
    usb_c?: boolean;
    ethernet?: boolean;
}
