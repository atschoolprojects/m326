export enum StorageType {
    HDD = 'hdd',
    SSD = 'ssd'
}
